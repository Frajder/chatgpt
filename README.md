# ChatGPT prompts

## System prompts of ChatGPT interface

System prompts that are integrated into the ChatGPT interface.

[ChatGPT 4 Default](system-prompts/ChatGPT4-default.txt)

[ChatGPT 4 Advanced Data Analysis](system-prompts/ChatGPT4-advanced-data-analysis.txt)

[ChatGPT 4 Web Browsing](system-prompts/ChatGPT4-dalle-3.txt)

[ChatGPT 4 with Custom instructions](system-prompts/ChatGPT4-custom-instructions.txt)
